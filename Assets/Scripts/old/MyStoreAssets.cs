﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Soomla.Store;

public class MyStoreAssets : IStoreAssets
{
	public const string GEM_CURRENCY_ID = "gem_currency_id";
	public const string GEM_CURRENCY_100PACK_ID = "gem_100_id";
	public const string PLAYER_LIFE_ID = "player_life_id";


	public int GetVersion ()
	{
		return 0;
	}
		
	public VirtualCurrency[] GetCurrencies ()
	{
		return new VirtualCurrency[]{GEM_CURRENCY};
	}
	
	public VirtualGood[] GetGoods ()
	{
		return new VirtualGood[] {PLAYER_LIFE_GOOD, NO_ADS_LTVG};
	}
	
	public VirtualCurrencyPack[] GetCurrencyPacks ()
	{
		return new VirtualCurrencyPack[] {HUND_GEM_PACK};
	}
	
	public VirtualCategory[] GetCategories ()
	{
		return new VirtualCategory[]{GENERAL_CATEGORY};
	}
	
	/** Virtual Currencies **/
	
	public static VirtualCurrency GEM_CURRENCY = new VirtualCurrency (
		"Gem",                               // Name
		"Gem used as currency",                      // Description
		GEM_CURRENCY_ID                    // Item ID
	);
	
	/** Virtual Currency Packs **/
	
	public static VirtualCurrencyPack HUND_GEM_PACK = new VirtualCurrencyPack (
		"100 Gems",                          // Name
		"100 gems units",            // Description
		GEM_CURRENCY_100PACK_ID,                       // Item ID
		100,                                  // Number of currencies in the pack
		GEM_CURRENCY_ID,                   // ID of the currency associated with this pack
		new PurchaseWithMarket (// Purchase type (with real money $)
	                       GEM_CURRENCY_100PACK_ID,                   // Product ID
	                       1.99                                   // Price (in real money $)
	)
	);
	
	/** Virtual Goods **/
	
	public static VirtualGood PLAYER_LIFE_GOOD = new SingleUseVG (
		"Life",                             // Name
		"Player life",      // Description
		PLAYER_LIFE_ID,                          // Item ID
		new PurchaseWithVirtualItem (// Purchase type (with virtual currency)
	                            GEM_CURRENCY_ID,                     // ID of the item used to pay with
	                            225                                    // Price (amount of coins)
	)
	);
	
	// NOTE: Create non-consumable items using LifeTimeVG with PurchaseType of PurchaseWithMarket.
	public static VirtualGood NO_ADS_LTVG = new LifetimeVG (
		"No Ads",                             // Name
		"No More Ads!",                       // Description
		"no_ads_ID",                          // Item ID
		new PurchaseWithMarket (// Purchase type (with real money $)
	                       "no_ads_PROD_ID",                      // Product ID
	                       0.99                                   // Price (in real money $)
	)
	);
	
	/** Virtual Categories **/
	
	public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory (
		"General", new List<string> (new string[] {PLAYER_LIFE_ID})
	);
}
