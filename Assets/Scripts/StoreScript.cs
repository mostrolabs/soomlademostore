﻿using UnityEngine;
using System.Collections;
using Soomla.Store;
using UnityEngine.UI;


public class StoreScript : MonoBehaviour
{
	public Text txtGems;
	public Text txtLives;
	public Text txtDebug;

	void Start ()
	{
		StoreEvents.OnSoomlaStoreInitialized += onSoomlaStoreInitialized;
		StoreEvents.OnCurrencyBalanceChanged += onCurrencyBalanceChanged;

		try {
			txtDebug.text = "starting...";
			SoomlaStore.Initialize (new MyOwnStoreAssets ());

		} catch (System.Exception ex) {
			txtDebug.text = ex.Message;
		}
	}

	void onSoomlaStoreInitialized ()
	{
		txtDebug.text = "started...";
		string vgs_id = "VG: ";
		foreach (VirtualGood vg in StoreInfo.Goods) {
			vgs_id += vg.ItemId + ",";
		}
	
		string vcps_id = "\nVCP: ";
		foreach (VirtualCurrencyPack vcp in StoreInfo.CurrencyPacks) {
			vcps_id += vcp.ItemId;
		}

		txtDebug.text = vgs_id + vcps_id;
		txtGems.text = StoreInventory.GetItemBalance ("currency_gem").ToString ();
		txtLives.text = StoreInventory.GetItemBalance ("player_life").ToString ();
	}

	void onCurrencyBalanceChanged (VirtualCurrency vc, int balance, int amount)
	{
		txtGems.text = StoreInventory.GetItemBalance ("currency_gem").ToString ();
		txtLives.text = StoreInventory.GetItemBalance ("player_life").ToString ();
	}

	public void Buy100Gems ()
	{
		try {
			StoreInventory.BuyItem ("gems_400");

		} catch (System.Exception ex) {
			txtDebug.text = ex.Message;
		}

	}

	public void BuyALife ()
	{
		try {
			StoreInventory.BuyItem ("player_life");
		} catch (System.Exception ex) {
			txtDebug.text = ex.Message;
		}

	}

	public void BuyNoAds ()
	{
		try {
			StoreInventory.BuyItem ("no_ads_ID");
		} catch (System.Exception ex) {
			txtDebug.text = ex.Message;
		}

	}
}