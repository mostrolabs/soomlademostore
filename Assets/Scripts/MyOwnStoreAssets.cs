using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;

public class MyOwnStoreAssets : IStoreAssets
{
	public int GetVersion ()
	{
		return 0;
	}

	public VirtualCurrency[] GetCurrencies ()
	{
		return new VirtualCurrency[]{GEM_CURRENCY};
	}

	public VirtualGood[] GetGoods ()
	{
		return new VirtualGood[] {PLAYERLIFE_GOOD, NO_ADS_LTVG};
	}

	public VirtualCurrencyPack[] GetCurrencyPacks ()
	{
		return new VirtualCurrencyPack[] {GEMS400_PACK, GEMS100_PACK};
	}

	public VirtualCategory[] GetCategories ()
	{
		return new VirtualCategory[]{GENERAL_CATEGORY};
	}

	/** Static Final Members **/

	public const string GEM_CURRENCY_ITEM_ID = "currency_gem";
	public const string GEM400_PACK_PRODUCT_ID = "gems_400"; //"android.test.purchased";
	public const string GEM1000_PACK_PRODUCT_ID = "gems_1000";
	public const string PLAYERLIFE_ITEM_ID = "player_life";
	public const string NO_ADS_LIFETIME_PRODUCT_ID = "no_ads";

	/** Virtual Currencies **/
	public static VirtualCurrency GEM_CURRENCY = new VirtualCurrency (
	            "Gems",										// name
	            "",					// description
	            GEM_CURRENCY_ITEM_ID						// item id
	);


	/** Virtual Currency Packs **/
	public static VirtualCurrencyPack GEMS400_PACK = new VirtualCurrencyPack (
	            "400 gems",                                  // name
	            "Test purchase of an item",                 	// description
	            "gems_400",                                  // item id
				400,                                            // number of currencies in the pack
				GEM_CURRENCY_ITEM_ID,                        // the currency associated with this pack
	            new PurchaseWithMarket (GEM400_PACK_PRODUCT_ID, 4.99)
	);

	public static VirtualCurrencyPack GEMS100_PACK = new VirtualCurrencyPack (
	            "1000 Gems",                                 // name
	            "Test item unavailable",                 		// description
	            "gems_1000",                                 // item id
				1000,                                           // number of currencies in the pack
				GEM_CURRENCY_ITEM_ID,                        // the currency associated with this pack
	            new PurchaseWithMarket (GEM1000_PACK_PRODUCT_ID, 8.99)
	);

	/** Virtual Goods **/
	public static VirtualGood PLAYERLIFE_GOOD = new SingleUseVG (
	            "Player Life",                                       		// name
	            "Life to continue playing", // description
	            "player_life",                                       		// item id
	            new PurchaseWithVirtualItem (GEM_CURRENCY_ITEM_ID, 225)); // the way this virtual good is purchased


	/** Virtual Categories **/
	public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory (
	            "General", new List<string> (new string[] {
		PLAYERLIFE_ITEM_ID,
	})
	);


	/** LifeTimeVGs **/
	public static VirtualGood NO_ADS_LTVG = new LifetimeVG (
			"No Ads", 														// name
			"No More Ads!",				 									// description
			"no_ads",														// item id
			new PurchaseWithMarket (NO_ADS_LIFETIME_PRODUCT_ID, 0.99));	// the way this virtual good is purchased
}
