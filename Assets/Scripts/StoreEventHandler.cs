﻿using System;
using System.Collections.Generic;
using Soomla.Store;
using UnityEngine;


public class StoreEventHandler
{		

	public StoreEventHandler ()
	{
		StoreEvents.OnMarketPurchase += onMarketPurchase;
		StoreEvents.OnMarketRefund += onMarketRefund;
		StoreEvents.OnItemPurchased += onItemPurchased;
		StoreEvents.OnGoodEquipped += onGoodEquipped;
		StoreEvents.OnGoodUnEquipped += onGoodUnequipped;
		StoreEvents.OnGoodUpgrade += onGoodUpgrade;
		StoreEvents.OnBillingSupported += onBillingSupported;
		StoreEvents.OnBillingNotSupported += onBillingNotSupported;
		StoreEvents.OnMarketPurchaseStarted += onMarketPurchaseStarted;
		StoreEvents.OnItemPurchaseStarted += onItemPurchaseStarted;
		StoreEvents.OnUnexpectedErrorInStore += onUnexpectedErrorInStore;
		StoreEvents.OnCurrencyBalanceChanged += onCurrencyBalanceChanged;
		StoreEvents.OnGoodBalanceChanged += onGoodBalanceChanged;
		StoreEvents.OnMarketPurchaseCancelled += onMarketPurchaseCancelled;
		StoreEvents.OnRestoreTransactionsStarted += onRestoreTransactionsStarted;
		StoreEvents.OnRestoreTransactionsFinished += onRestoreTransactionsFinished;
		StoreEvents.OnSoomlaStoreInitialized += onSoomlaStoreInitialized;
		#if UNITY_ANDROID && !UNITY_EDITOR
			StoreEvents.OnIabServiceStarted += onIabServiceStarted;
			StoreEvents.OnIabServiceStopped += onIabServiceStopped;
		#endif
	}
		

	public void onMarketPurchase (PurchasableVirtualItem pvi, string payload, Dictionary<string, string> extra)
	{
			
	}

	public void onMarketRefund (PurchasableVirtualItem pvi)
	{
			
	}

	public void onItemPurchased (PurchasableVirtualItem pvi, string payload)
	{
			
	}

	public void onGoodEquipped (EquippableVG good)
	{
			
	}

	public void onGoodUnequipped (EquippableVG good)
	{
			
	}

	public void onGoodUpgrade (VirtualGood good, UpgradeVG currentUpgrade)
	{
			
	}

	public void onBillingSupported ()
	{
			
	}

	public void onBillingNotSupported ()
	{
			
	}

	public void onMarketPurchaseStarted (PurchasableVirtualItem pvi)
	{
			
	}

	public void onItemPurchaseStarted (PurchasableVirtualItem pvi)
	{
			
	}

	public void onMarketPurchaseCancelled (PurchasableVirtualItem pvi)
	{
			
	}

	public void onUnexpectedErrorInStore (string message)
	{
			
	}

	public void onCurrencyBalanceChanged (VirtualCurrency virtualCurrency, int balance, int amountAdded)
	{
			
	}
		
	public void onGoodBalanceChanged (VirtualGood good, int balance, int amountAdded)
	{
		Debug.Log ("Good balance changed");
	}

	public void onRestoreTransactionsStarted ()
	{
			
	}
		
	public void onRestoreTransactionsFinished (bool success)
	{
			
	}
		
	public void onSoomlaStoreInitialized ()
	{
			
	}
		
		#if UNITY_ANDROID && !UNITY_EDITOR
		public void onIabServiceStarted() {
			
		}
		public void onIabServiceStopped() {
			
		}
		#endif
}